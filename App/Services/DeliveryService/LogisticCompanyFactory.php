<?php

declare(strict_types=1);

namespace App\Services\DeliveryService;

abstract class LogisticCompanyFactory
{
    abstract protected function addTransport(string $transportType) : IDelivery;
}
