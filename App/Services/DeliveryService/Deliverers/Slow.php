<?php

declare(strict_types=1);

namespace App\Services\DeliveryService\Deliverers;

use App\Services\DeliveryService\IDelivery;
use JsonException;

use function date;
use function json_decode;
use function json_encode;

class Slow extends Delivery implements IDelivery
{
    private string $baseUrl = 'https://slow.com/';
    private float $basePrice = 150;
    private float $coefficient = 1.1;

    public function delivery(): string
    {
        return 'Slow Delivery';
    }

    /**
     * @throws \Exception
     */
    public function calculationCost(string $sourceKladr, string $targetKladr, float $weight): array
    {
        $this->prepareData($this->getData($sourceKladr, $targetKladr, $weight));

        return [
            'price' => $this->price,
            'date' => $this->date,
            'error' => $this->error
        ];
    }

    /**
     * @throws JsonException
     */
    public function getData(string $sourceKladr, string $targetKladr, float $weight): string
    {
        return json_encode([
            'coefficient' => $this->coefficient,
            'date' => date('Y-m-d'),
            'error' => 'php  ' . $this->delivery()
        ], JSON_THROW_ON_ERROR);
    }

    /**
     * @throws \Exception
     */
    private function prepareData(string $data): void
    {
        $data = json_decode($data, false, 512, JSON_THROW_ON_ERROR);

        if (empty($data->coefficient)) {
            throw new \RuntimeException('data False');
        }

        $this->price = $this->preparePrice($data->coefficient);
        $this->error = $data->error;
        $this->date = $data->date;
    }

    private function preparePrice(float $price): float
    {
        return $this->basePrice * $price;
    }
}
