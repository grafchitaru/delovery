<?php

declare(strict_types=1);

namespace App\Services\DeliveryService\Deliverers;

use App\Services\DeliveryService\IDelivery;
use JsonException;

use function date;
use function json_decode;
use function json_encode;
use function time;

class Fast extends Delivery implements IDelivery
{
    private string $baseUrl = 'https://fast.com/';
    private float $coefficient = 500;

    public function delivery(): string
    {
        return 'Fast Delivery';
    }

    /**
     * @throws \Exception
     */
    public function calculationCost(string $sourceKladr, string $targetKladr, float $weight): array
    {
        $this->prepareData($this->getData($sourceKladr, $targetKladr, $weight));

        return [
            'price' => $this->price,
            'date' => $this->date,
            'error' => $this->error
        ];
    }

    /**
     * @throws JsonException
     */
    public function getData(string $sourceKladr, string $targetKladr, float $weight): string
    {
        return json_encode([
            'price' => $this->coefficient,
            'period' => 1,
            'error' => 'Error ' . $this->delivery()
        ], JSON_THROW_ON_ERROR);
    }

    /**
     * @throws \Exception
     */
    private function prepareData(string $data): void
    {
        $data = json_decode($data, false, 512, JSON_THROW_ON_ERROR);

        if (empty($data->price)) {
            throw new \Exception('data False');
        }

        $this->price = $data->price;
        $this->error = $data->error;
        $this->date = $this->preparePeriod($data->period);
    }

    private function preparePeriod(int $period): string
    {
        return date('Y-m-d', time() + ($period + (date('H') < 18  ? 0 : 1)) * 60 * 60 * 24);
    }
}
