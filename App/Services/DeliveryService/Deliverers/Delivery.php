<?php

declare(strict_types=1);

namespace App\Services\DeliveryService\Deliverers;

class Delivery
{
    protected float $price;
    protected string $date;
    protected string $error;
}