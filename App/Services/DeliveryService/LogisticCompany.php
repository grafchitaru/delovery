<?php

declare(strict_types=1);

namespace App\Services\DeliveryService;

class LogisticCompany extends LogisticCompanyFactory
{
    private array $loaded = [];
    private string $transportType;

    /**
     * @throws \Exception
     */
    public function addTransport(string $transportType): IDelivery
    {
        $this->setTransportType($transportType);
        $this->checkIsAlreadyLoaded();
        $this->setLoadedTransport();

        return $this->getTransport();
    }

    private function setTransportType(string $transportType): void
    {
        $this->transportType = $transportType;
    }

    private function checkIsAlreadyLoaded(): void
    {
        if(isset($this->loaded[$this->transportType]) === true) {
            throw new \Exception($this->transportType . __('Already Loaded'));
        }
    }

    private function setLoadedTransport(): void
    {
        $this->loaded[$this->transportType] = $this->getTransport();
    }

    private function getTransport()
    {
        return new ('App\\Services\\DeliveryService\\Deliverers\\' . $this->transportType);
    }
}
