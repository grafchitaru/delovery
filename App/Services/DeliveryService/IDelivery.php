<?php

declare(strict_types=1);

namespace App\Services\DeliveryService;

interface IDelivery
{
    public function delivery(): string;

    public function calculationCost(string $sourceKladr, string $targetKladr, float $weight):  array;

    public function getData(string $sourceKladr, string $targetKladr, float $weight): string;
}
